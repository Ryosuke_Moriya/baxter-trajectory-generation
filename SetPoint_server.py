#!/usr/bin/env python
import struct
import sys
import rospy
import baxter_interface
from ros_start.srv import(
    BaxterSetPoint,
    BaxterSetPointResponse
)
import time
from baxter_interface import CHECK_VERSION
from IKmod import IKsolve
from mover import Mover
from baxter_pykdl import baxter_kinematics
import numpy as np
import math
import string

class server:
    def __init__(self):

        baxter = baxter_interface.RobotEnable()
        baxter.enable()
        self.limb = "right"
        self.right = baxter_interface.Limb(self.limb)
        self.right.set_joint_position_speed(0.3)
        self.control_cmd = dict()
        print("set neutral position...")
        baxter_interface.Limb.move_to_neutral(self.right)
        print("OK")
        self.IKservice = IKsolve(self.limb)
        self.count = 0
        self.mover = Mover()
        self.t = 0


    def ik_response(self,req):
        self.coordinate = [req.x,req.y,req.z]
        self.limb_joints = self.IKservice.solve(self.coordinate)
        if(req.mode == "playback"):
            if (self.limb_joints != 0):
                if (req.flag == 1):
                    self.mover.map_file(loops = 1)
                    time.sleep(5)
                    self.right.move_to_neutral()
                self.count += 1
                if(req.flag == 1):
                    self.count = 0
                return BaxterSetPointResponse(True)
            else:
                if(req.flag == 1):
                    self.mover.map_file(loops = 1)
                    time.sleep(5)
                    self.right.move_to_neutral()
                return BaxterSetPointResponse(False)

        if(req.mode == "record"):
            if (self.limb_joints != 0):
                if(self.count == 0):
                    self.fls = open("record.rec","w")
                    self.right.move_to_joint_positions(self.limb_joints,5)
                self.right.set_joint_positions(self.limb_joints)
                self.make_file(req.flag)
                self.count += 1
                if(req.flag == 1):
                    self.count = 0
                    time.sleep(5)
                    self.right.move_to_neutral()
                return BaxterSetPointResponse(True)
            else:
                return BaxterSetPointResponse(False)


    def time_gene(self):
        pose = self.right.endpoint_pose()
        pt_now = str(pose["position"]).translate(string.maketrans("",""),
                        "xyz= ()Point").split(",")

        dst = math.sqrt((self.coordinate[0]-float(pt_now[0]))**2
                        +(self.coordinate[1]-float(pt_now[1]))**2
                        +(self.coordinate[2]-float(pt_now[2]))**2)
        time = dst/50
        return time

    def make_file(self,flag):
        if (self.count == 0):
            self.fls.write("time,right_s0,right_s1,right_e0,right_e1,right_w0,right_w1,right_w2\n")

        self.t += self.time_gene()
        cnt = [str(self.t),self.limb_joints["right_s0"],
                self.limb_joints["right_s1"],self.limb_joints["right_e0"],
                self.limb_joints["right_e1"],self.limb_joints["right_w0"],
                self.limb_joints["right_w1"],self.limb_joints["right_w2"]]
        self.fls.write(str(cnt).translate(string.maketrans("",""),"[]' ")+"\n")
        if(flag == 1):
            self.t = 0
            self.fls.close()

if __name__ == "__main__":
    rospy.init_node("a")
    srv = server()
    service_server = rospy.Service("set_point",BaxterSetPoint,srv.ik_response)
    rospy.spin()
