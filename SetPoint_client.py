#!/usr/bin/env python

import rospy
import sys

from ros_start.srv import BaxterSetPoint
import argparse

def main():
    parser = argparse.ArgumentParser("Mode select")
    parser.add_argument("-r", action = "store_true",help = "record mode.")
    parser.add_argument("-p", action = "store_true",help = "playback mode.")
    args = parser.parse_args()
    rospy.init_node("Point_client")
    set_point = rospy.ServiceProxy("set_point",BaxterSetPoint)
    line_count = sum(1 for line in open("coodinate.txt"))
    count = 0
    flag = 0
    if (args.r):
        mode = "record"
    if (args.p):
        mode = "playback"
    f = open("coodinate.txt","r")
    for line in f:
        count += 1
        l = line.split(",")
        x = float(l[0])
        y = float(l[1])
        z = float(l[2])
        if (count == line_count):
            flag = 1
        response = set_point(x,y,z,flag,line_count,mode)
        if response.success:
            rospy.loginfo("set [%f,%f,%f] success."%(x,y,z))
        else:
            rospy.logerr("set [%f,%f,%f] failed."%(x,y,z))



    f.close()
    sys.exit()

if __name__ == "__main__":
    main()
